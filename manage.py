#!/usr/bin/env python
# encoding: utf-8
import subprocess
import sys

from flask_script import Manager
from flask_script.commands import ShowUrls

from application.app import create_app

manager = Manager(create_app)

manager.add_command("showurls", ShowUrls())


@manager.shell
def make_shell_context():
    from application.extensions import redis
    return dict(app=manager.app, rds=redis)


@manager.command
def simple_run():
    app = create_app()
    app.run(debug=True)


@manager.command
def lint():
    """Runs code linter."""
    lint = subprocess.call(['flake8', '--ignore=E402,F403,E501', 'application/',
                            'manage.py', 'tests/']) == 0
    if lint:
        print('OK')
    sys.exit(lint)

if __name__ == "__main__":
    manager.run()
