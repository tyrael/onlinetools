#!/usr/bin/env python
# encoding: utf-8
from srv import srv_bp
from index import index_bp

all_bps = [
    srv_bp,
    index_bp,
]
