#!/usr/bin/env python
# encoding: utf-8
from flask import Blueprint, request, make_response, current_app

from application.extensions import redis
from application.utils.const import ONE_MONTH, REDIS_EXPIRE, REDIS_PREFIX
from application.service.rsted.html import rst2html as _rst2html
from application.service.rsted.pdf import rst2pdf as _rst2pdf


srv_bp = Blueprint('srv', __name__, url_prefix='/srv')




@srv_bp.route('/rst2html/', methods=['POST', 'GET'])
def rst2html():
    rst = request.form.get('rst', '')
    theme = request.form.get('theme')
    if theme == 'basic':
        theme = None
    html = _rst2html(rst, theme=theme)
    return html


@srv_bp.route('/rst2pdf/', methods=['POST'])
def rst2pdf():
    rst = request.form.get('rst', '')
    theme = request.form.get('theme')
    if theme == 'basic':
        theme = None

    pdf = _rst2pdf(rst, theme=theme)
    responce = make_response(pdf)
    responce.headers['Content-Type'] = 'application/pdf'
    responce.headers['Content-Disposition'] = 'attachment; filename="rst.pdf"'
    responce.headers['Content-Transfer-Encoding'] = 'binary'
    return responce


@srv_bp.route('/save_rst/', methods=['POST'])
def save_rst():
    rst = request.form.get('rst')
    if not rst:
        return ''

    from hashlib import md5

    md5sum = md5(rst).hexdigest()
    redis_key = '%s%s' % (REDIS_PREFIX, md5sum)

    redis_expire = redis.get(REDIS_EXPIRE)
    if redis.setnx(redis_key, rst) and redis_expire:
        redis.expire(redis_key, REDIS_EXPIRE)
    response = make_response(md5sum)
    response.headers['Content-Type'] = 'text/plain'
    return response

@srv_bp.route('/del_rst/', methods=['GET'])
def del_rst():
    saved_id = request.args.get('n')
    if saved_id:
        redis_key = '%s%s' % (REDIS_PREFIX, saved_id)
        redis.delete(redis_key)

    response = make_response()
    response.headers['Content-Type'] = 'text/plain'
    return response
