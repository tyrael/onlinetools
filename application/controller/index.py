#!/usr/bin/env python
# encoding: utf-8
from flask import Blueprint, request, render_template

from application.extensions import redis, render_html
from application.utils.const import REDIS_PREFIX


index_bp = Blueprint('index', __name__)


@index_bp.route("/")
@render_html('index.html')
def index():
    yield 'js_params', {'theme': request.args.get('theme', '')}

    saved_doc_id = request.args.get('n')
    if saved_doc_id:
        rst = redis.get('%s%s' % (REDIS_PREFIX, saved_doc_id))
        if rst:
            yield 'rst', rst
            yield 'document', saved_doc_id


@index_bp.route('/about/')
def about():
    return render_template('about.html')
