#!/usr/bin/env python
# encoding: utf-8
from flask import request, url_for


def view_is_active(view_name):
    if request.path == url_for(view_name):
        return 'active'
    return ''
