#!/usr/bin/env python
# encoding: utf-8

# time constants
ONE_MINUTE = 60
ONE_HOUR = 60 * ONE_MINUTE
ONE_DAY = 24 * ONE_HOUR
ONE_MONTH = 30 * ONE_DAY

# redis constants
REDIS_EXPIRE = "REDIS_EXPIRE"
REDIS_PREFIX = "REDIS_PREFIX"
