#!/usr/bin/env python
# encoding: utf-8
from flask_redis import FlaskRedis

from .helpers import render_html


redis = FlaskRedis()
