#!/usr/bin/env python
# encoding: utf-8
# all the imports
import os
import sys

from flask import Flask

from config.config import Config
from application.controller import all_bps
from application.extensions import redis


reload(sys)
sys.setdefaultencoding('utf-8')


def create_app():
    # create our little application :)
    app = Flask(__name__)
    app.config.from_object(Config)

    config_extensions(app)
    config_blueprint(app)
    config_context(app)

    return app


def config_blueprint(app):
    for bp in all_bps:
        app.register_blueprint(bp)


def config_extensions(app):
    redis.init_app(app)


def config_context(app):
    from application.utils.helper import view_is_active

    @app.context_processor
    def ctx_pro():
        return {
            'MEDIA_URL': '/static/',
            'is_active': view_is_active
        }


if __name__ == '__main__':
    app = create_app()
    app.run(host=app.config.get('HOST', '0.0.0.0'),
            port=app.config.get('PORT', 5000))
