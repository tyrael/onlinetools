#!/usr/bin/env python
# encoding: utf-8
import os


class Config(object):
    """Base config class."""
    # Flask app config
    DEBUG = True
    TESTING = False
    SECRET_KEY = "sample_key"

    HOST = "0.0.0.0"
    PORT = 5000

    # Root path of project
    PROJECT_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

    # redis config
    REDIS_URL = "redis://:@localhost:6379/0"
